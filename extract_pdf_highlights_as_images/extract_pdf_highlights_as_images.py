#!/usr/bin/env python3

"""Script to export regular highlights in PDF files as PNG files."""

import fitz
from hashlib import sha256

prefix = "book - "

doc = fitz.open("input.pdf")
inst_counter = 0
for pi in range(doc.page_count):
    page = doc[pi]
    inst = page.first_annot
    inst_counter = 0

    five_percent_height = (page.rect.br.y - page.rect.tl.y) * 0.05

    while inst:
        if inst.type[0] in (8, 9, 10, 11):  # one of the 4 types above
            inst_counter += 1
            # print(inst.get_text("dict"))

            # define a suitable cropping box which spans the whole page
            # and adds padding around the highlighted text.
            tl_pt = fitz.Point(
                page.rect.tl.x,
                max(page.rect.tl.y, inst.rect.tl.y - five_percent_height),
            )
            br_pt = fitz.Point(
                page.rect.br.x,
                min(page.rect.br.y, inst.rect.br.y + five_percent_height),
            )
            hl_clip = fitz.Rect(tl_pt, br_pt)
            id_str = str(hl_clip)
            id_hash = sha256(bytes(id_str, "utf-8")).hexdigest()[:5]
            # Reduce the opacity of the highlighted passage.
            inst.set_opacity(0.5)
            # Set the blend mode to "Multiply" to avoid affecting the text
            # been highlighted.
            inst.set_blendmode("Multiply")
            inst.update()
            # Increase the scale to obtain an image with a larger resolution.
            scale_x, scale_y = 8, 8

            zoom_mat = fitz.Matrix(scale_x, scale_y)
            pix = page.get_pixmap(matrix=zoom_mat, clip=hl_clip)
            pix.save(
                f"{prefix}page-{pi:04d}-annotation-{inst_counter:02d}-{id_hash}.png"
            )
        # None returned after last annot
        inst = inst.next

doc.close()
