===========
Auto-agenda
===========

Traverse Markdown files located in a given directory and look for lines
following a specific syntax to create a list of tasks and to-do items.

A much more mature alternative:
`Org-agenda <https://orgmode.org/manual/Agenda-Views.html>`_ with usual
`Org-mode <https://arxiv.org/search/advanced>`_ files or with
`Org-roam <https://www.orgroam.com/>`_/`Md-roam <https://github.com/nobiot/md-roam>`_
nodes.

TODO
====
- Remove tasks done after a specific threshold.
- Improve configuration.
- Rename functions.
- Use objects to make it easier to track conditions. Examples:
  - If above the age threshold: “remaining” -> “ago”
  - Auto singular/plural
