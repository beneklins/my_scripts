#!/usr/bin/env python3

"""Auto-agenda: parse Markdown files to create an agenda with tasks/to-dos.

Copyright (c) 2021–2022 Igor Benek-Lins <physics@ibeneklins.com>

Licence
-------
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public Licence as published by the Free Software
Foundation, either version 3 of the Licence, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public Licence for more details.

You should have received a copy of the GNU General Public Licence along with
this program. If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import configparser
import os
import re
from numpy import inf
from datetime import datetime, timedelta

parser = argparse.ArgumentParser(
    description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "-c",
    "--config",
    help="",
    nargs=1,
    dest="config_file",
    default="auto-agenda.conf",
)
args = parser.parse_args()

## Read the main configuration file preserving the case.
config = configparser.RawConfigParser()
config.optionxform = lambda option: option
config.read(args.config_file)

workspace_dir = config["config"]["workspace"]
skip_files = eval(config["config"]["skip_files"])
spacing_step = int(config["config"]["spacing_step"])
done_flag = config["config"]["done_flag"]
done_tag = config["config"]["done_tag"]
skip_tags = eval(config["config"]["skip_tags"])
warning_flags = eval(config["config"]["warning_flags"])
working_flags = eval(config["config"]["working_flags"])
task_signature = config["config"]["task_signature"]
subtask_signature = config["config"]["subtask_signature"]
todo_tag_regexp = config["config"]["todo_tag_regexp"]
todo_tag = config["config"]["todo_tag"]
date_regexp = config["config"]["date_regexp"]
old_entries_threshold = int(config["config"]["old_entries_threshold"])


def argsort(seq):
    # http://stackoverflow.com/questions/3382352/equivalent-of-numpy-argsort-in-basic-python/3382369#3382369
    # by unutbu
    return sorted(range(len(seq)), key=seq.__getitem__)


def parse_item(item, main=False):
    title, info, file = item

    done = done_flag if done_tag in info else " "

    file_link = f"[[{os.path.splitext(file)[0]}]]"

    try:
        creation_date = datetime.strptime(file.split("-")[1], "%Y-%m-%d")
        num_days_since_creation_date = (datetime.now() - creation_date).days
    except:
        creation_date = "ERROR"
        num_days_since_creation_date = 0
    if num_days_since_creation_date > 1:
        plural = "s"
    else:
        plural = ""
    days_since_creation_date = (
        f"{num_days_since_creation_date} day{plural} ago"
    )

    project = ""
    bold = False
    wip = False

    tags = []
    if main:
        creation_tags = [f"Created {days_since_creation_date}"]
        # [datetime.strftime(creation_date, "%Y-%m-%d")]
    else:
        creation_tags = []

    info_list = info.strip().split(" ")
    deadline = ""
    days_until_deadline = ""
    num_days_until_deadline = inf
    for info_bit in info_list:
        if "project:" in info_bit:
            project = info_bit.split(":")[-1]
        if "deadline:" in info_bit:
            deadline_list = info_bit.split(":")
            if len(deadline_list) <= 2:
                break
            deadline = datetime.strptime(deadline_list[-1], "%Y-%m-%d")
            num_days_until_deadline = (deadline - datetime.now()).days
            # Add 18/24 because the hour is set to 00 and the resulting
            # day count does not make sense, as nothing [citation needed]
            # is due on the 00h00min of the deadline day.
            num_days_until_deadline += 18 / 24
            extra_hours_until_deadline = round(
                ((deadline - datetime.now()).seconds) / (60 * 60 * 24), 1
            )
            if abs(num_days_until_deadline) > 1:
                plural = "s"
            else:
                plural = ""
            date_to_show = (
                num_days_until_deadline + extra_hours_until_deadline
            )
            if date_to_show >= 0:
                days_until_deadline_desc = "remaining"
            else:
                days_until_deadline_desc = "ago"
            days_until_deadline = (
                f"{date_to_show:.1f} day{plural} {days_until_deadline_desc}"
            )
        elif info_bit[0] == "#":
            if info_bit not in skip_tags:
                tags.append(info_bit)
                creation_tags.append(info_bit)
            if info_bit in warning_flags:
                bold = True
            if info_bit in working_flags:
                wip = True

    if bold and done != done_flag:
        title = f"__{title}__"
    # if project:
    #   title = f"[[{project}]]: {title}"

    return dict(
        done=done,
        file_link=file_link,
        creation_date=creation_date,
        days_since_creation_date=days_since_creation_date,
        num_days_since_creation_date=num_days_since_creation_date,
        num_days_until_deadline=num_days_until_deadline,
        project=project,
        wip=wip,
        deadline=deadline,
        days_until_deadline=days_until_deadline,
        tags=tags,
        creation_tags=creation_tags,
        title=title,
    )


def list_item(list_of_item, list_of_subitems):
    space = " " * spacing_step * 0
    next_space = " " * spacing_step * 1

    output_text_creation = ""
    output_text_no_creation = ""
    subtasks_text = ""
    task_data = parse_item(list_of_item, main=True)
    subtask_data = [parse_item(subitem) for subitem in list_of_subitems]
    num_days_until_deadline = task_data["num_days_until_deadline"]

    ## Time view.
    if task_data["project"]:
        project = f"[[{task_data['project']}]] "
    else:
        project = ""
    if task_data["days_until_deadline"]:
        time_line = f"__`{task_data['days_until_deadline']}`__\n"
        has_deadline = True
    else:
        time_line = f"`No deadline, created {task_data['num_days_since_creation_date']} days ago`\n"
        has_deadline = False
    if task_data["wip"]:
        wip = True
    else:
        wip = False
    main_line = f"{time_line}{space}- [{task_data['done']}] {project}{task_data['title']} {task_data['file_link']}"
    extra_line = f"{next_space}{' '.join(task_data['tags'])}"
    extra_line_creation = (
        f"{next_space}{' '.join(task_data['creation_tags'])}"
    )

    for subtask in subtask_data:
        sub_line = f"{next_space}- [{subtask['done']}] {subtask['title']}"
        subtasks_text += sub_line
        subtasks_text += "\n"

        if subtask["tags"] or subtask["creation_tags"]:
            extra_sub_line = f"{next_space*2}{' '.join(subtask['tags'])}"
            subtasks_text += extra_sub_line
        subtasks_text += "\n"

    output_text_no_creation = f"{main_line}\n{extra_line}\n"
    output_text_creation = f"{main_line}\n{extra_line_creation}\n"

    output_text_creation += subtasks_text
    output_text_no_creation += subtasks_text
    # print(output_text_no_creation)

    done = True if task_data["done"] == done_flag else False

    return (
        wip,
        has_deadline,
        num_days_until_deadline,
        output_text_creation,
        output_text_no_creation,
        project,
        done,
    )


def get_tags_payees():
    """
    Walk over workspace_dir, read all task*.md files and extract information
    using regexp.
    """
    data = []
    todo = []

    for root, dirs, files in os.walk(workspace_dir):
        for file in files:
            # if file.startswith("task") and file.endswith(".md"):
            if file.endswith(".md") and file not in skip_files:
                with open(os.path.join(root, file), "r") as f:
                    file_content = f.read()
                    file_lines = file_content.split("\n")

                    task = list()
                    subtasks = list()

                    info_line_idx = -1
                    for idx, line in enumerate(file_lines):
                        if re.match(task_signature, line):
                            info_line_idx = idx + 1
                            info_line = file_lines[idx + 1]
                            task_title = line.split(task_signature)[
                                -1
                            ].strip()
                            task = [task_title, info_line, file]
                        if info_line_idx != -1 and re.match(
                            subtask_signature, line
                        ):
                            subtask_title = line.split(subtask_signature)[
                                -1
                            ].strip()
                            subinfo_line = file_lines[idx + 1]
                            subtasks.append(
                                (subtask_title, subinfo_line, file)
                            )
                        todo_position = re.findall(todo_tag_regexp, line)
                        if todo_position and info_line_idx == -1:
                            todo_done_flag = True if "[x]" in line else False
                            todo_done = "[x]" if todo_done_flag else "[ ]"
                            # todo_title = ''.join(line.split(todo_done)[1:]).strip()
                            todo_title = re.sub(
                                todo_tag_regexp, "", line, count=0, flags=0
                            )
                            # Remove todo signature and any spaces before it.
                            todo_title = re.sub(
                                r"\s*-\s*\[[\sx]\] ",
                                "",
                                todo_title,
                                count=0,
                                flags=0,
                            )
                            todo_deadline_list = re.findall(
                                rf"deadline:{date_regexp}", todo_title
                            )
                            # Remove deadline from title
                            todo_title = re.sub(
                                rf"deadline:{date_regexp}",
                                "",
                                todo_title,
                                count=0,
                                flags=0,
                            )
                            if todo_deadline_list:
                                todo_deadline = todo_deadline_list[0].split(
                                    ":"
                                )[-1]
                            else:
                                todo_deadline = ""
                            todo_info = (
                                todo_done,
                                todo_title,
                                file,
                                todo_deadline,
                                todo_done_flag,
                            )
                            todo.append(todo_info)

                    if task:
                        data.append(list_item(task, subtasks))
                    # if subtasks:
                    #   print(f"{SPACING}- Sub-tasks")
                    # for subtask in subtasks:
                    #   list_item(subtask, level=1)

                    # break

    deadline_is_over = list()
    deadline_is_over_idx = list()

    deadline_close_but_not_started = list()
    deadline_close_but_not_started_idx = list()

    deadline_close_wip = list()
    deadline_close_wip_idx = list()

    deadline_this_month_wip = list()
    deadline_this_month_wip_idx = list()

    deadline_further_away_not_started = list()
    deadline_further_away_not_started_idx = list()

    deadline_further_away_wip = list()
    deadline_further_away_wip_idx = list()

    done_list = list()
    done_list_idx = list()

    wip_deadline_not_set = list()
    projects = dict()

    total_data = len(data)

    data_within_threshold = list()
    total_data_within_threshold = 0
    for item in data:
        num_days_until_deadline, done = item[2], item[6]
        if not done or num_days_until_deadline + old_entries_threshold >= 0:
            data_within_threshold.append(item)
            total_data_within_threshold += 1

    data_processed = 0
    for item in data_within_threshold:
        (
            wip,
            has_deadline,
            num_days_until_deadline,
            text,
            text_no_creation,
            project,
            done,
        ) = item

        if done:
            done_list.append(text_no_creation)
            done_list_idx.append(num_days_until_deadline)
            data_processed += 1
        elif num_days_until_deadline < 0:
            deadline_is_over.append(text_no_creation)
            deadline_is_over_idx.append(num_days_until_deadline)
            data_processed += 1
        elif num_days_until_deadline <= 28:
            if not wip:
                deadline_close_but_not_started.append(text_no_creation)
                deadline_close_but_not_started_idx.append(
                    num_days_until_deadline
                )
                data_processed += 1
            else:
                if num_days_until_deadline <= 14:
                    deadline_close_wip.append(text_no_creation)
                    deadline_close_wip_idx.append(num_days_until_deadline)
                    data_processed += 1
                else:
                    deadline_this_month_wip.append(text_no_creation)
                    deadline_this_month_wip_idx.append(
                        num_days_until_deadline
                    )
                    data_processed += 1
        elif has_deadline and num_days_until_deadline > 28:
            if wip:
                deadline_further_away_wip.append(text_no_creation)
                deadline_further_away_wip_idx.append(num_days_until_deadline)
                data_processed += 1
            else:
                deadline_further_away_not_started.append(text)
                deadline_further_away_not_started_idx.append(
                    num_days_until_deadline
                )
                data_processed += 1
        elif not has_deadline:
            wip_deadline_not_set.append(text)
            data_processed += 1

    if data_processed != total_data_within_threshold:
        print("# WARNING: NOT ALL DATA WAS PROCESSED")
        print("# WARNING: NOT ALL DATA WAS PROCESSED")
        print("# WARNING: NOT ALL DATA WAS PROCESSED")
        print("# WARNING: NOT ALL DATA WAS PROCESSED\n")

    print("# Agenda\n")

    todos_to_hide = list()
    if todo:
        print("## To do\n")
        all_todo = len(todo)
        for todo_item in todo:
            (
                todo_done,
                todo_title,
                todo_file,
                todo_deadline,
                todo_done_flag,
            ) = todo_item
            todo_to_show = False
            deadline_str = ""
            if todo_deadline:
                deadline_todo = datetime.strptime(todo_deadline, "%Y-%m-%d")
                num_days_until_deadline_todo = (
                    deadline_todo - datetime.now()
                ).days
                # Add 18/24 because the hour is set to 00 and the resulting
                # day count does not make sense, as nothing [citation needed]
                # is due on the 00h00min of the deadline day.
                num_days_until_deadline_todo += 18 / 24
                extra_hours_until_deadline = round(
                    ((deadline_todo - datetime.now()).seconds)
                    / (60 * 60 * 24),
                    1,
                )
                if num_days_until_deadline_todo > 1:
                    plural = "s"
                else:
                    plural = ""
                todo_title = re.sub(
                    r" deadline:\d+", "", todo_title, count=0, flags=0
                )
                deadline_str = f"`{num_days_until_deadline_todo+extra_hours_until_deadline:.1f} day{plural} to go` "
            todo_title = re.sub(
                f" {todo_tag}", "", todo_title, count=0, flags=0
            )
            file_link = f"[[{os.path.splitext(todo_file)[0]}]]"
            if not todo_deadline and not todo_done_flag:
                todo_to_show = True
            if todo_deadline:
                if (
                    not todo_done_flag
                    or num_days_until_deadline_todo + old_entries_threshold
                    >= 0
                ):
                    todo_to_show = True

            # print(f"- {todo_done} {deadline_str}{todo_title} {file_link}")
            if todo_to_show:
                print(f"- {todo_done} {deadline_str}{todo_title} {file_link}")
            else:
                todos_to_hide.append(
                    f"- {todo_done} {deadline_str}{todo_title} {file_link}"
                )

        print("\n")

    # print("# Time line\n")

    if deadline_is_over:
        print("## ** DEADLINE IS OVER **\n")
        for i in argsort(deadline_is_over_idx):
            print(deadline_is_over[i])
    if deadline_close_but_not_started:
        print("##  Deadline is close, but not started\n")
        for i in argsort(deadline_close_but_not_started_idx):
            print(deadline_close_but_not_started[i])
    if done_list:
        print("## Finished!\n")
        for i in argsort(done_list_idx):
            print(done_list[i])
    if deadline_close_wip:
        print("##  Within two weeks\n")
        for i in argsort(deadline_close_wip_idx):
            print(deadline_close_wip[i])
    if deadline_this_month_wip:
        print("## This month\n")
        for i in argsort(deadline_this_month_wip_idx):
            print(deadline_this_month_wip[i])

    if deadline_further_away_wip or deadline_further_away_not_started:
        print("## Deadline further away\n")
        if deadline_further_away_wip:
            print("### Working on it\n")
            for i in argsort(deadline_further_away_wip_idx):
                print(deadline_further_away_wip[i])
        if deadline_further_away_not_started:
            print("###  Need to start working on it sometime\n")
            for i in argsort(deadline_further_away_not_started):
                print(deadline_further_away_not_started[i])

    if wip_deadline_not_set:
        print("## Deadline not set\n")
        for i in wip_deadline_not_set:
            print(i)

    print(
        f"\n\n<!-- Showing {data_processed}/{total_data} tasks. We hide finished tasks after {old_entries_threshold} days of their deadline -->"
    )

    if todo and todos_to_hide:
        print(f"\n\n\n## Past to-dos {len(todos_to_hide)}/{all_todo}\n")
        print("\n".join(todos_to_hide))


get_tags_payees()
