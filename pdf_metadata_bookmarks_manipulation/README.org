- [[#bc5aeb32-6da5-4d8f-8cb4-9a171b182e14][How to edit PDF metadata?]]
- [[#6879184a-ce07-40ea-b09f-510443b3d136][How to insert bookmarks by hand in a PDF file?]] (uses =generate_pdf_bookmarks.py=)
- [[#8a0e4c31-29e1-4479-a251-224cfc59d9a9][How to change the page numbering of a PDF file]]

* How to edit PDF metadata?
:PROPERTIES:
:CUSTOM_ID:       bc5aeb32-6da5-4d8f-8cb4-9a171b182e14
:END:
1. Get the PDF metadata using PDFtk
   #+begin_src shell
     pdftk "pdf_file.pdf" dump_data > "pdf_metadata.dat"
   #+end_src

2. Edit the =InfoKey= fields in the metadata. Some possible keys are
    - =InfoKey: Title=
    - =InfoKey: Subject=
    - =InfoKey: Keywords=
    - =InfoKey: Author=
    - =InfoKey: Creator=
    - =InfoKey: Producer=
    - =InfoKey: CreationDate=
   Where, the values are set in the =InfoValue= field after each =InfoKey=:
   #+begin_src ascii
     InfoKey: Title
     InfoValue: The title goes here
   #+end_src

3. Create a PDF file with the new metadata using
   #+begin_src shell
    pdftk "pdf_file.pdf" update_info "pdf_metadata.dat" output "pdf_file.new.pdf"
   #+end_src

4. Voilá.

* How to insert bookmarks by hand in a PDF file?
:PROPERTIES:
:CUSTOM_ID:       6879184a-ce07-40ea-b09f-510443b3d136
:END:
Adapted from the original by pyrocrasty, https://superuser.com/a/915399.

1. Create the table of contents using the following format, where the page number is the “hard page number”, the one that goes from 1 to the total number of pages in the PDF file, not the “human/virtual page number”:
   #+begin_src ascii
     BookmarkBegin
     BookmarkTitle: Introduction
     BookmarkLevel: 1
     BookmarkPageNumber: 1
     BookmarkBegin
     BookmarkTitle: Chapter I
     BookmarkLevel: 1
     BookmarkPageNumber: 5
     BookmarkBegin
     BookmarkTitle: Section I
     BookmarkLevel: 2
     BookmarkPageNumber: 6
     BookmarkBegin
     BookmarkTitle: Subsection I
     BookmarkLevel: 2
     BookmarkPageNumber: 6
     BookmarkBegin
     BookmarkTitle: Chapter II
     BookmarkLevel: 1
     BookmarkPageNumber: 9
   #+end_src
   Use the =generate_pdf_bookmarks.py= script.

2. Get the PDF metadata using
   #+begin_src shell
     pdftk pdf_file.pdf dump_data > pdf_metadata.dat
   #+end_src

3. In the metadata file find the line that begins with =NumberOfPages= and insert the table of contents after that line.

4. Create a PDF file with the new metadata using
   #+begin_src shell
     pdftk "pdf_file.pdf" update_info "pdf_metadata.dat" output "pdf_file.new.pdf"
   #+end_src

5. Voilá.

* How to change the page numbering of a PDF file
:PROPERTIES:
:CUSTOM_ID:       8a0e4c31-29e1-4479-a251-224cfc59d9a9
:END:
Open the PDF file in a text editor and search for =/Catalog=. Then follow the scheme:
#+begin_src ascii
  1 0 obj
  <<
  /Type /Catalog
      /PageLabels << /Nums [
          0 << /P (cover) >> % Labels the 1st page with the string “cover”
          1 << /S /r >>      % Pages 2-3 with lowercase Roman numerals
          4 << /S /D >>      % Pages 5-7 with Arabic numerals
          7 << /S /D         % Pages 8-... with Arabic numerals with...
               /P (A-)       % the prefix “A-”...
               /St 8         % starting at page 8
               >>
      ]
      >>
  ... whatever was here ...
>>
endobj
#+end_src
