#!/bin/env python3

"""
./generate_pdf_bookmarks.py INPUT_FILE "SHIFT"

The INPUT_FILE has to be formatted as follows:
page_number\t1 Section
page_number\t\t1.1 Subsection
page_number\t\t\t1.1.1 Subsubsection
page_number\t\t\t\t1.1.1.1 Subsubsubsection

The SHIFT will add/subtract a number from the page number. This is useful
when the book has a proper and sequential page numbering.

The output file will have a new extension ".pdf_bookmarks"
"""

import sys

input_filename = sys.argv[1]
shift = int(sys.argv[2])

output_filename = input_filename + ".pdf_bookmarks"

input_file = open(input_filename, "r")
output_file = open(output_filename, "w")

output = ""

for line in input_file:

    line_array = line.split("\t")
    print(line_array)

    page_number = int(line_array.pop(0)) + shift
    remaining_elements = len(line_array)

    level = 1
    for i in range(remaining_elements):
        element = line_array.pop(0)
        if element != "":
            title = element.replace("\n", "")
            output = (
                output
                + "BookmarkBegin\nBookmarkTitle: %s\nBookmarkLevel: %d\nBookmarkPageNumber: %d\n"
                % (str(title), level, page_number)
            )
        else:
            level = level + 1

output_file.write(output)

input_file.close()
output_file.close()
