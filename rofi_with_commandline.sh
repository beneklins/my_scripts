#!/bin/bash

# rofi with drun, run. If pressing Shift+<Enter>, the command is sent to a terminal.
rofi -show combi -combi-modi drun,run -modi combi -run-shell-command '{terminal} -e bash -ic "{cmd} && read"'
