from ics import Calendar
import requests
from bs4 import BeautifulSoup
# TODO: Remove use of Numpy in favour of
#       https://stackoverflow.com/questions/3382352/equivalent-of-numpy-argsort-in-basic-python/3382369#3382369
import numpy as np

input_calendar = "output_calendar.ics"
with open(input_calendar, 'r') as calendar:
    c = Calendar(calendar.read())

org_file_contents = ""
entries = []
titles = []

org_header = "* USAPS March Meeting 2022\n\n"
org_file_contents += org_header

session_prefix = "**"
title_prefix = "***"
abstract_prefix = "Abstract ::"
authors_prefix = "Authors ::"
url_prefix = "URL (video search) ::"

for _ in range(len(list(c))):
    try:
        item = c.events.pop()
    except:
        break
    title = item.name
    authors_desc_ses = str(item.description).split('\n')
    session = authors_desc_ses[0]
    abstract = '\n'.join(authors_desc_ses[1:-1])
    authors = authors_desc_ses[-1]
    url = item.location
    session_entry = f"{session_prefix} {session}"
    entry = f"""{title_prefix} {title}
{authors_prefix} {authors}
{url_prefix} {url}
{abstract_prefix} {abstract}
\nNotes\n\n\n"""
    entries.append((session_entry, entry))
    titles.append(title)

sorted_entries = np.array(entries)[np.argsort(titles)]

# Populate dictionary with empty lists
dict_sessions = dict()
for s, e in sorted_entries:
    dict_sessions[s] = []

for s, e in sorted_entries:
    dict_sessions[s].append(e)

for s in dict_sessions.keys():
    org_file_contents += f"{s}\n\n"
    for e in dict_sessions[s]:
        org_file_contents += f"{e}"

with open('output_calendar_org_file.org', 'w') as f:
    f.write(org_file_contents)
