===============================
USAPS “My Scheduler” ICS parser
===============================

Parse USAPS “My Scheduler” ICS to add information about each presentation
since the events in the original exported ICS file do not have enough
information.

Additional information (title, authors and abstract) is extracted from the
webpage of each abstract.

An example of how the “My Scheduler” exported ICS looks like can be found in
the `examples` folder, as well the associated parsed ICS file.

Made in the occasion of the US-American Physical Society (USAPS) March Meeting
2022 (the original script dates from the USAPS March Meeting 2021)

Screenshots
===========

This is how original events look like:

.. image:: examples/screenshot_original_event.png
  :width: 500
  :alt: Original event as shown in KOrganizer

This is how the same event looks like when parsed using this script:

.. image:: examples/screenshot_parsed_event.png
  :width: 500
  :alt: Parsed event as shown in KOrganizer
