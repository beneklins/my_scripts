#!/bin/sh
# Check if we can ping ${HOST}==$1. If not, restart ${CONNECTION}==$2 using
# nmcli. On every check, output info messages to the journal.
# Run it as a cronjob.

HOST="${1}"
CONNECTION="${2}"
TAG="nm_connection_check"

function is_net_active () {
	ping -c3 ${1} >/dev/null 2>&1 && echo up || echo down
}

case $( is_net_active ${HOST} ) in
	'up')
		echo "Connection \"${CONNECTION}\" is up." | systemd-cat -t "${TAG}"
	;;
	'down')
		echo "Connection \"${CONNECTION}\" is down. Trying to reconnect..." | systemd-cat -t "${TAG}" -p err

		nmcli c down ${CONNECTION}
		nmcli c up ${CONNECTION}

		case $( is_net_active ${HOST} ) in
			'up')
				echo "Connection \"${CONNECTION}\" is back up." | systemd-cat -t "${TAG}"
			;;
			'down')
				echo "Connection \"${CONNECTION}\" is still down..." | systemd-cat -t "${TAG}" -p err
			;;
		esac
	;;
esac
