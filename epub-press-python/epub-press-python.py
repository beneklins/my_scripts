#!/usr/bin/env python3

"""
TODO
----
- Remove URLs that were correctly parsed, whilst keeping the one that
  did not. We should warn about the latter.
- Only include in the compilation file the URLs that were correctly parsed.
- Store the URLs used to generate the Epub files in a file.
"""

import argparse
from time import localtime, strftime
from bs4 import BeautifulSoup
from unicodedata import normalize as unicodedata_normalize
import requests
import re
from json import loads as json_loads
import subprocess
import urllib
import time
from pathlib import Path

VERBOSE = False
API = "http://localhost:5000/api/v1/books"
HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:100.0) Gecko/20100101 Firefox/100.0",
    "Accept-Encoding": "identity",
    "Accept": "text/xml,application/xml,application/xhtml+xml,text/html;q=0.9",
    "Connection": "Keep-Alive",
    "Accept-Language": "en-us,en;q=0.5",
    "Accept-Charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.7",
    "Keep-Alive": "300",
}
# We do not assume nvm to be active using their loading snippet
# (https://github.com/nvm-sh/nvm#profile_snippet). Hence, we need the
# location of nvm’s nvm.sh file.
# TODO: Turn this into a CLI argument.
NVM_SH = "/usr/share/nvm/init-nvm.sh"


def today(with_time=False, now=False):
    """Return date and/or time."""
    if with_time:
        return strftime("%Y-%m-%d_%Hh%Mmin", localtime())
    if now:
        return strftime("%Hh%Mmin%Ss", localtime())
    return strftime("%Y-%m-%d", localtime())


def download_file(url, dest):
    """Download `url` to `dest` using `urllib`."""
    response = urllib.request.urlopen(url)
    data = response.read()

    # Write data to file.
    file_ = open(dest, "b+w")
    file_.write(data)
    file_.close()


def slugify(value):
    """Slugify string, allowing Unicode characters.

    Convert spaces or repeated dashes to single dashes. Remove characters
    that are not alphanumerics, underscores or hyphens. Convert to
    lowercase. Also strip leading and trailing whitespace, dashes and
    underscores.

    Original code from the Django project. See the LICENCE file for
    licensing details.
    """
    value = str(value)
    value = unicodedata_normalize("NFKC", value)
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


def get_title(url, domain):
    """Get the title of `url` from its HTML title tag.

    If we are not able to obtain the title from the title tag, use the
    “basename” of the URL.

    """
    # Get tittle
    page = requests.get(url, headers=HEADERS)
    # TODO: Check if webpage was found (not-404) or can be parsed.
    title = BeautifulSoup(page.content, "html.parser").find("title").text

    if not isinstance(title, str):
        # Get the last part of the URL.
        re_result = re.findall(r"(?:.+?\/)([a-zA-Z0-9\.\-_]*)", url)
        title = re_result[-1] if re_result else url

    return title


def get_domain(url):
    """Get the `url`’s domain."""
    re_result = re.findall(
        r"(?:http[s]?\:\/\/(?:www\.)?)?([a-zA-Z0-9\.\-_]*?)\/.*", url
    )

    if re_result:
        domain = re_result[0]
    else:
        domain = "unknown"

    return domain


class EpubPressProcess:
    """Class to launch epub-press and send URLs for parsing.

    To launch, interact, get output in real time and terminate a process,
    we follow Eli Bendersky’s “Interacting with a long-running child
    process in Python”, available at
    https://web.archive.org/web/20211213160028/https://eli.thegreenplace.net/2017/interacting-with-a-long-running-child-process-in-python/

    """

    def __init__(self, args):
        self.epub_press_dir = args.epub_press
        self.database = args.input_file
        self.output_dir = args.output_dir
        self.timeout = args.timeout
        self.compilation = args.compilation
        # TODO: Use a temporary log file?
        self.log_file = Path(
            f"{self.output_dir}/epub-press-python-{today(with_time=True)}.log"
        ).resolve()
        # Get Node.js version from epub-press’ .nvmrc file.
        self.nvmrc_path = Path(f"{self.epub_press_dir}/.nvmrc").resolve()
        try:
            with open(self.nvmrc_path, "r") as nvmrc:
                self.node_version = int(nvmrc.read())
        except IOError:
            print(f"Error whilst reading {self.nvmrc_path}")
        except ValueError:
            print(
                f"Error whilst getting Node.js version. Check {self.nvmrc_path}."
            )

        with open(self.database, "r") as file:
            self.urls = []
            self.urls_quoted = []
            for line in file.readlines():
                if line.strip():
                    self.urls.append(line.strip())
                    self.urls_quoted.append(f'"{line.strip()}"')
        self.number_of_articles = len(self.urls)

        self.epub_press_proc = self.start_server()

        if self.compilation:
            self.generate_compilation()
        else:
            self.generate_individual_files()

        self.stop_server()

    def check_if_published(self, id):
        """
        Watch for epub-press’ characteristic string of a published “book”.

        Read the `self.log_file` until we find “Book Published id=X”, which
        indicates that the URL was properly parsed and published by
        epub-press. As of late-2022, this string can be found in
        epub-press’ `routes/api/books-v1.js` file.
        """
        id_string = f"Book Published id={id}"
        with open(self.log_file) as f:
            line_count = sum(1 for _ in f)
        log = open(self.log_file)

        for i in range(line_count):
            log.readline()

        success = False
        break_flag = False
        start_time = time.time()
        while not break_flag:
            try:
                line = log.readline()
                if line:
                    if "verbose" in line and VERBOSE:
                        print(f"\t[debug] {line}", end="")
                    if id_string in line:
                        if VERBOSE:
                            print("Book published!")
                        break_flag = True
                        success = True
                if time.time() - start_time >= self.timeout:
                    print(
                        "Timeout. Check log file and/or increase the timeout."
                    )
                    break_flag = True
                    self.stop_server()
                    self.start_server()
            except:
                break_flag = True
                self.stop_server()
                self.start_server()
                print("Break!")

        log.close()

        return success

    def output_reader(self, outq):
        for line in iter(self.proc.stdout.readline, b""):
            outq.put(line.decode("utf-8"))

    def start_server(self):
        """Start the epub-press process."""
        print(f"Starting epub-press server. Log file: {self.log_file}")
        self.proc = subprocess.Popen(
            f"source {NVM_SH} && nvm use {self.node_version} && npm start > {self.log_file}",
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            cwd=self.epub_press_dir,
            shell=True,
        )
        time.sleep(5)

    def stop_server(self):
        """Stop the server by sending a SIGINT (<Ctrl-c>)."""
        self.proc.terminate()
        time.sleep(1)

    def send_job(self, title, author, urls):
        """Send request to generate a book to epub-press."""
        data = {
            "title": title,
            "description": "",
            "author": author,
            "genre": "",
            "coverPath": "",
            "urls": urls,
        }
        # A 202 response is a good response. We should add a check for that.
        # 500 is bad.
        response = requests.post(
            API,
            headers={
                "Content-Type": "application/json",
            },
            json=data,
        )
        if VERBOSE:
            print(f"[debug] epub-press response: {response.content}")
        # Get the ID of the job (“book”).
        epub_id = json_loads(response.content.decode("utf-8"))["id"]

        return epub_id

    def generate_compilation(self):
        """Generate a single file with all articles."""
        print(f"Generating compilation with a timeout of {self.timeout}.")
        epub_id = self.send_job(
            title=f"Coletânea de artigos {today()}", author="", urls=self.urls
        )
        epub_single = self.check_if_published(epub_id)
        if epub_single:
            filename = slugify(f"compilation-{today()}")
            output_filename = Path(
                f"{self.output_dir}/{filename}.epub"
            ).resolve()
            download_url = f"{API}/{epub_id}/download"
            download_file(download_url, output_filename)
            print(f"Downloaded to {output_filename}")

    def generate_individual_files(self):
        """Generate individual Epub files, one for each URL."""
        individual_urls_success = []
        individual_urls_failed = []
        for idx, url in enumerate(self.urls):
            print(
                f"[{idx+1:02d}/{self.number_of_articles:02d}] Individual Epub for {url}"
            )
            domain = get_domain(url)
            title = get_title(url, domain)
            filename = slugify(f"{today()}_-_{domain}_-_{title}").lower()
            # In a shell, we need to escape single quotes inside
            # single-quoted-strings in a different manner.
            # See https://unix.stackexchange.com/a/75997
            title = title.replace("'", r"'\''")
            title = title.replace('"', r"\"")
            url_as_list = list([url])

            try:
                epub_id = self.send_job(
                    title=title, author=domain, urls=url_as_list
                )
                epub_single = self.check_if_published(epub_id)
                if epub_single:
                    output_filename = Path(
                        f"{self.output_dir}/{filename}.epub"
                    ).resolve()
                    download_url = f"{API}/{epub_id}/download"
                    download_file(download_url, output_filename)
                    print(
                        f"[{idx+1:02d}/{self.number_of_articles:02d}] Downloaded to {output_filename}"
                    )
                    individual_urls_success.append(url)
                else:
                    individual_urls_failed.append(url)
            except:
                individual_urls_failed.append(url)

        now_str = today(now=True)
        urls_success_log = (
            f"{self.output_dir}/{today()}-{now_str}_-_urls_okay.log"
        )
        urls_failed_log = (
            f"{self.output_dir}/{today()}-{now_str}_-_urls_failed.log"
        )
        if individual_urls_success:
            with open(urls_success_log, "w") as file:
                file.write("\n".join(individual_urls_success))
        if individual_urls_failed:
            with open(urls_failed_log, "w") as file:
                file.write("\n".join(individual_urls_failed))

        print("\nepub-press has finished parsing the URLs.")
        if individual_urls_failed:
            print("The following URLs were not properly parsed:")
            for url in individual_urls_failed:
                print(url)


if __name__ == "__main__":
    # Command-line arguments.
    parser = argparse.ArgumentParser(description="epub-press-python")
    parser.add_argument(
        "-e",
        "--epub-press",
        action="store",
        dest="epub_press",
        help="Directory where the epub-press repository is located.",
    )
    parser.add_argument(
        "-i",
        "--input",
        action="store",
        dest="input_file",
        help="File containing the URLs (one per line).",
    )
    parser.add_argument(
        "-o",
        "--output",
        action="store",
        dest="output_dir",
        help="Directory where the Epub files should be placed.",
    )
    parser.add_argument(
        "-c",
        "--compilation",
        action="store_true",
        dest="compilation",
        default=False,
        help=(
            "Generates a compilation, a single Epub file, instead of individual"
            " Epub files. Default: False"
        ),
    )
    parser.add_argument(
        "-t",
        "--timeout",
        action="store",
        dest="timeout",
        default=600,
        help=(
            "Time (in seconds) to wait for epub-press to generate each file."
            " Important when creating compilations."
        ),
    )
    args = parser.parse_args()

    epub_press = EpubPressProcess(args)
