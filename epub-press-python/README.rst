=================
epub-press-python
=================

Python script to generate Epub files from URLs listed in a file using
`epub-press <https://github.com/haroldtreen/epub-press>`_.

Works in UNIX-like systems with POSIX-compliant shells.

Requirements
------------
* External software

  * epub-press and its related dependencies (npm and nvm are required)

* Python

  * See `requirements.txt`
