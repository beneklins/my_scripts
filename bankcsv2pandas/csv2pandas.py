#!/usr/bin/env python3

"""
csv2pandas

Copyright © 2020-2022 Igor Benek-Lins <physics@ibeneklins.com>.

Converte os CSV do CIBC, KOHO e Transferwise para um dataframe do Pandas,
fazendo as substituições definidas em um arquivo de configuração, que é então
convertido para um CSV a ser utilizado para fazer uma Feijoada.

TODO
----

· Diferenciar transações de crédito ou débito para poder usar
  target_account e original_account corretamente.

"""

from os import listdir
from time import strftime
import argparse
import configparser
import pandas as pd
import numpy as np

BANKS = ["cibc", "koho", "wise"]
CSV_COLUMNS_CIBC = ["date", "payee", "D", "C", "original_account"]
EXTRA_COLUMNS_CIBC = [
    "translated_account",
    "account",
    "target_acc_0",
    "target_acc_0_amount",
    "description",
]
CSV_COLUMNS_KOHO = ["date", "payee", "C", "D", "balance", "notes"]
EXTRA_COLUMNS_KOHO = [
    "original_account",
    "translated_account",
    "account",
    "target_acc_0",
    "target_acc_0_amount",
    "description",
]
CSV_COLUMNS_WISE = [
    "transaction_id",
    "date",
    "amount",
    "currency",
    "description",
    "payment_ref",
    "balance",
    "from_currency",
    "to_currency",
    "currency_exchange_rate",
    "payer_wise",
    "payee_wise",
    "account_number",
    "payee",
    "original_account",
    "card_holder_name",
    "attachment",
    "note",
    "total_fees",
]
EXTRA_COLUMNS_WISE = [
    "translated_account",
    "account",
    "target_acc_0",
    "target_acc_0_amount",
    "description",
]

timestamp = strftime("%Y%m%d-%H%M%S")

csv_columns = dict(
    cibc=CSV_COLUMNS_CIBC,
    koho=CSV_COLUMNS_KOHO,
    wise=CSV_COLUMNS_WISE,
)
csv_columns_extra = dict(
    cibc=EXTRA_COLUMNS_CIBC,
    koho=EXTRA_COLUMNS_KOHO,
    wise=EXTRA_COLUMNS_WISE,
)

# Command-line arguments.
parser = argparse.ArgumentParser(description="Configuration")
parser.add_argument(
    "-c",
    "--config",
    action="store",
    dest="config_file",
    default="csv2pandas.conf",
    help="Path to the configuration file. Default: `csv2pandas.conf`",
)
args = parser.parse_args()
config_file = args.config_file

# Read configuration file, preserving the case.
config = configparser.RawConfigParser()
config.optionxform = lambda option: option
config.read(config_file)

# General parameters
input_folder = config["general"]["input_folder"]

# Convert the configparser object into a dictionary.
# Original by fzzylogic, https://stackoverflow.com/a/57024021
#             Licence: CC BY-SA 4.0
config_data = {
    i: {i[0]: i[1] for i in config.items(i)}
    for i in config.sections()
    if i != "general"
}

contas = {
    # CIBC
    # Credit card-like.
    r".*cibc.*account1.*csv": "Liabilities:CIBC:Account1",
    # Checking account-like.
    r".*cibc.*account2.*csv": "Assets:CIBC:Account2",
    # KOHO
    r".*koho.*personal.*csv": "Assets:KOHO:Personal",
    # Transferwise
    r".*wise.*account1.*csv": "Assets:Wise:Account1",
}

# Merge credit (C) and debit (D) columns.
def merge_value_columns(df, bank):
    if bank == "wise":
        return df
    df.loc[:, "D"] *= -1
    if bank == "cibc":
        df["D"] = df["D"].combine_first(df["C"])
    elif bank == "koho":
        df["D"] = df["D"] + df["C"]
    df.rename(columns={"D": "amount"}, inplace=True)
    del df["C"]
    return df


def main(config_data, contas, input_folder):

    # Import CSV files and join them into a pandas dataframe.
    input_files = [input_folder + "/" + i for i in listdir(input_folder)]
    df = pd.DataFrame()
    for csv_file in input_files:
        if "cibc" in csv_file:
            bank = "cibc"
            skip_rows = 0
        elif "koho" in csv_file:
            bank = "koho"
            skip_rows = 1
        elif "wise" in csv_file:
            bank = "wise"
            skip_rows = 1

        dfi = pd.read_csv(
            csv_file, names=csv_columns[bank], skiprows=skip_rows
        )
        # Obtém conta de origem a partir da coluna `original_account`. Caso
        # contrário utilize o nome do arquivo CSV de entrada.
        # dfi["original_account"] = dfi["original_account"].fillna(csv_file)
        # Para evitar colocar identificadores numéricos no dicionário `contas`,
        # utilizemos apenas o nome do arquivo de entrada.
        dfi["original_account"] = csv_file
        # dfi["original_account"] = csv_file.replace(f"{input_folder}/", "")
        output_columns = csv_columns[bank] + csv_columns_extra[bank]
        cols = dfi.columns.tolist()
        for i in output_columns:
            if i not in cols:
                dfi[i] = np.full(len(dfi), "")
        dfi = dfi[output_columns]

        # Lowercase all entries.
        dfi = dfi.applymap(lambda s: s.lower() if isinstance(s, str) else s)

        # Identify source account.
        for item in contas.keys():
            rows = dfi.index[
                dfi["original_account"].str.contains(item, regex=True)
            ].tolist()
            translation = contas[item]
            dfi.loc[rows, "translated_account"] = translation

        if bank == "cibc":
            pass
        elif bank == "koho":
            # Keep only YYYY-MM-DD from date
            dfi["date"] = [iso.split(" ")[0] for iso in dfi["date"].tolist()]
            # Remove KOHO extra columns.
            del dfi["balance"]
            del dfi["notes"]
        elif bank == "wise":
            # Reverse DD-MM-YYYY to ISO format.
            dfi["date"] = [
                "-".join(date.split("-")[::-1])
                for date in dfi["date"].tolist()
            ]
            dfi["payee"] = dfi["payee"].fillna("")
            dfi["translated_account"] = (
                dfi["translated_account"] + ":" + dfi["currency"].str.upper()
            )

        # Merge debit and credit amount columns.
        dfi = merge_value_columns(dfi, bank=bank)

        df = pd.concat([df, dfi]).reset_index(drop=True)

    # Sort by date and then amount.
    df = df.sort_values(by=["date", "amount"], ignore_index=True)
    # Copy amount to target_acc_0
    df["target_acc_0_amount"] = df["amount"]
    # Add currency column.
    # TODO: Tratar moeda a partir do (1) banco e (2) exceções.
    #       Por exemplo, banco A tem como moeda padrão $1, mas algumas
    #       contas podem ser $2.
    df["currency"] = np.full(len(df), "CAD")
    # Add ignore_transaction, revised and tags columns.
    df["ignore_transaction"] = np.full(len(df), False)
    df["revised"] = np.full(len(df), False)
    df["tags"] = np.full(len(df), "")

    # If an entry is positive, copy the translated account to the target account.
    rows_positive = df.index[df["amount"] > 0].tolist()
    df.loc[rows_positive, "target_acc_0"] = df.iloc[rows_positive][
        "translated_account"
    ]
    # If an entry is negative, copy the translated account to the source account.
    rows_negative = df.index[df["amount"] <= 0].tolist()
    df.loc[rows_negative, "account"] = df.iloc[rows_negative][
        "translated_account"
    ]

    # Identify payee.
    df["comment"] = (
        "transaction:"
        + df["payee"]
        + " amount:"
        + df["amount"].astype(str)
        + " file:"
        + df["original_account"]
    )
    for regex in config_data:
        rows = df.index[df["payee"].str.contains(regex)].tolist()
        for column_name in config_data[regex]:
            df.loc[rows, column_name] = config_data[regex][column_name]

    # We do not need the sign of the amounts.
    df["amount"] = abs(df["amount"])
    df["target_acc_0_amount"] = abs(df["target_acc_0_amount"])

    # Return values
    return df


output = main(config_data, contas, input_folder)
output.to_csv(f"cibc2csv_{timestamp}.csv")
